# yalf README

https://bitbucket.org/skoe/yalf

## What is it?

yalf is yet another LPC flasher. I made it for myself because I was not
satisfied with what I found out there.

So what is special about this one?

* It's written in Python, which means that
* it's easy to customize (e.g. to add not-yet-supported chips)
* easy to integrate into Makefiles or build scripts
* Runs on Linux and Windows (and all other platforms which support 
  pySerial)
* Contains a small "terminal" for debug output
* Makes use of DTR and RTS to reset the target and to enter the ISP 
  mode

## Supported chips

Following chips are supported at the moment. Additional ones can be
added easily by extending the file config.py.

* LPC1311, LPC1313, LPC1342, LPC1343
* LPC1769, LPC1768, LPC1767, LPC1766, LPC1765, LPC1764
* LPC1759, LPC1758, LPC1756, LPC1754, LPC1752, LPC1751

## Dependencies

* python 2.x - sorry, no 3.x port done yet
* pySerial (python-serial)

## Usage

One way is to copy the yalf package (i. e. the inner directory yalf) 
someehere into your project tree and start yalf.py with a relative path
from your build script.

```
python yalf.py [options] [actions]...

Connect to an LPCxxxx boot loader over a serial port and perform
an ISP action.

The full functionality is possible when DTR is connected to the reset
line and RTS is used to enter ISP command handler (e.g. PIO0_1).
If these connections do not exist, the user must enter the ISP command 
handler manually.

Options:
   --help (-h)         Show this usage message
   --verbose (-v)      Run in verbose mode
   --port              Select the serial port to be used (/dev/ttyS0)

Actions:
   erase <from> <to>   Erase flash sectors <from> to <to> (including)
   eraseall            Erase all flash sectors
   program <file>      Write a binary to flash
   ramload <file>      Write a binary to RAM (0x10000400)
   dump <from> <to>    Print a hex dump of memory content
   reset               Reset target using DTR line
   terminal            Reset and start a 115200 8N1 terminal w/o flow control,
                       this should be the last action.
                       Currently this terminal is RX only.
```

## License

```
# (c) 2011 - 2014 Thomas 'skoe' Giesel
#
# https://bitbucket.org/skoe/yalf
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
```
