#!/usr/bin/env python
#
# (c) 2011 - 2014 Thomas 'skoe' Giesel
#
# https://bitbucket.org/skoe/yalf
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
 
import sys, os, binascii, array, getopt
from collections import deque

import config
import lpcll

version = '1.1'

class Yalf:

    def __init__(self, verbose, port, bitrate):
        self._verbose = verbose
        self._port = port
        self._bitrate = bitrate
        self.lpcll = lpcll.LPCLowLevel(verbose, port, bitrate)

    ###########################################################################
    def _find_int32_type(self):
        i = array.array('I')
        l = array.array('L')
        if i.itemsize == 4:
            self._int32_type = 'I'
        elif l.itemsize == 4:
            self._int32_type = 'L'
        else:
            sys.stderr.write("\nNo 32 bit integer type found :(\n")
            exit(1)

    ###########################################################################
    def print_words(self, addr, words):
        '''Print an array of 32-bit words.
        
        Print a hexdump of an array of 32-bit words WORDS to stdout.
        Use ADDR as the start address in the dump.
        
        '''
        offset = 0
        size = len(words)
        while offset < size:
            print '%08x: ' % (addr + 4 * offset),
            for i in range(0, 4):
                if offset < size:
                    print '%08x ' % words[offset],
                    offset += 1
            print ''


    ###########################################################################
    def checksum(self, data_bytes):
        sum = 0
        for v in data_bytes:
            sum += ord(v)
        return sum


    ###########################################################################
    def read_mem(self, start, end):
        '''Read memory content from device.
        
        Read N_BYTES bytes from the device memory at START. Return an array
        of 32-bit words.
        
        Everything is counted in 32-bit units here.
        '''
        self._find_int32_type()
        if start % 4:
            raise ValueError('Start address is not a aligned to 32-bit')
        if end < start:
            raise ValueError('End address lower than start address')
        n_words = (end - start + 3) / 4
        if self._verbose:
            print 'Read ' + str(n_words) + ' 32-bit words of memory at ' + str(start)
        addr      = start
        remaining = n_words
        words     = array.array(self._int32_type)
        while remaining > 0:
            block_size = 40 / 4
            if block_size > remaining:
                block_size = remaining
            cmd = 'R ' + str(addr) + ' ' + str(block_size * 4)
            response = self.lpcll.send_cmd_recv_response(cmd)
            self.lpcll.check_return_code(response)
            rx_ok = False
            while not rx_ok:
                line = self.lpcll.recv_response()
                # remove padding
                line = line[0:((ord(line[0]) - ord(' ')) * 4 + 2) / 3 + 1]
                binary = binascii.a2b_uu(line)
                mysum = self.checksum(binary)
                check = int(self.lpcll.recv_response())
                if check == mysum:
                    self.lpcll.send_cmd('OK')
                    words.extend(array.array(self._int32_type, binary))
                    remaining -= block_size
                    addr += block_size * 4
                    rx_ok = True
                else:
                    self.send_cmd('RESEND')
                    print 'Checksum error, retrying...'
        return words


    ###########################################################################
    def write_mem(self, start, data):
        '''
        Everything is counted in 32-bit units here.
        '''
        remaining = len(data)
        print 'Write ' + str(remaining) + ' 32-bit words to memory'
        offset = 0
        while remaining > 0:
            block_size = 40 / 4
            if block_size > remaining:
                block_size = remaining
            cmd = 'W ' + str(start + 4 * offset) + ' ' + str(4 * block_size)
            response = self.lpcll.send_cmd_recv_response(cmd)
            self.lpcll.check_return_code(response)
            block = data[offset : offset + block_size]
            mysum = self.checksum(block.tostring())
            text = binascii.b2a_uu(block).strip('\n')
            tx_ok = False
            while not tx_ok:
                self.lpcll.send_cmd(text)
                response = self.lpcll.send_cmd_recv_response(str(mysum))
                if response == 'OK':
                    tx_ok = True
            offset += block_size
            remaining -= block_size
        

    ###########################################################################
    def unlock(self):
        response = self.lpcll.send_cmd_recv_response('U 23130')
        self.lpcll.check_return_code(response)


    ###########################################################################
    def erase_sectors(self, start, end):
        '''Erase sectors.
        
        Erase all sectors from START to END, both are included in the range.
        If END is -1, erase to last sector.
        
        '''
        self.unlock()
        if end == -1:
            end = self.lpcll.get_num_sectors() - 1
        start = str(start)
        end   = str(end)
        if self._verbose:
            print 'Erasing sectors ' + start + ' to ' + end + '...'
        response = self.lpcll.send_cmd_recv_response('P ' + start + ' ' + end)
        self.lpcll.check_return_code(response)
        response = self.lpcll.send_cmd_recv_response('E ' + start + ' ' + end)
        self.lpcll.check_return_code(response)


    ###########################################################################
    def go(self, addr):
        cmd = 'G ' + str(addr) + ' T' # todo: ARM/Thumb
        response = self.lpcll.send_cmd_recv_response(cmd)
        self.lpcll.check_return_code(response)

    ###########################################################################
    def reset(self):
        print 'Resetting...'
        self.lpcll.reset()

    ###########################################################################
    def terminal(self):
        self.lpcll.terminal()

    ###########################################################################
    def ram_to_flash(self, dest, source, size):
    	sector_number = self.lpcll.sector_number_for_offset(dest)
        response = self.lpcll.send_cmd_recv_response(
                'P %u %u' % (sector_number, sector_number))
        self.lpcll.check_return_code(response)
        cmd = 'C ' + str(dest) + ' ' + str(source) + ' ' + str(size)
        response = self.lpcll.send_cmd_recv_response(cmd)
        self.lpcll.check_return_code(response)


    ###########################################################################
    def program_flash(self, filename):
        self.unlock()
        self._find_int32_type()
        try:
            f = open(filename, 'rb')
            data_bytes = f.read()
        except IOError, e:
            sys.stderr.write('Error: %s\n' % str(e))
            return # todo: error code
        size = len(data_bytes)
        print 'Size of input file:', size
        # pad to multiple of 4 bytes
        if size % 4 != 0:
            print 'Warning: Input padded to full 32 bit words'
            new_size = 4 * ((size + 3) / 4)
            data_bytes += (new_size - size) * '\0'
            size = new_size
        data = array.array(self._int32_type, data_bytes)
        boot_check = 0
        for val in data[0:7]:
            boot_check -= val
        if data[7] != (boot_check & 0xffffffff):
            data[7] = boot_check & 0xffffffff
            print 'Checksum updated'
        if self._verbose:
            print 'Writing',
        offset = 0
        while size:
            block_size = 256
            if block_size > size:
                block_size = size
            self.write_mem(0x10000400,
                           data[offset / 4 : (offset + block_size) / 4])
            self.ram_to_flash(offset, 0x10000400, 256)
            if self._verbose:
                print '.',
            size -= block_size
            offset += block_size
        if self._verbose:
            print


    ###########################################################################
    def program_ram(self, filename):
        self._find_int32_type()
        try:
            f = open(filename, 'rb')
            data_bytes = f.read()
        except IOError, e:
            sys.stderr.write('Error: %s\n' % str(e))
            return # todo: error code
        size = len(data_bytes)
        print 'Size of input file:', size
        # pad to multiple of 4 bytes
        if size % 4 != 0:
            print 'Warning: Input padded to full 32 bit words'
            data_bytes += (4 - size % 4) * '\0'
        data = array.array(self._int32_type, data_bytes)
        self.write_mem(0x10000400, data)


###############################################################################
def usage_and_exit(errmsg=None):
    stream = errmsg and sys.stderr or sys.stdout
    stream.write('''Usage: %(prog)s [options] [actions]...

Connect to an LPCxxxx boot loader over a serial port and perform
an ISP action. If no action is given, try to detect the device only.

The full functionality is possible when DTR is connected to the reset
line and RTS is used to enter ISP command handler (e.g. PIO0_1).
If these connections do not exist, the user must enter the ISP command 
handler manually.

Options:
   --help (-h)         Show this usage message
   --verbose (-v)      Run in verbose mode
   --port              Select the serial port to be used (/dev/ttyS0)

Actions:
   erase <from> <to>   Erase flash sectors <from> to <to> (including)
   eraseall            Erase all flash sectors
   program <file>      Write a binary to flash
   ramload <file>      Write a binary to RAM (0x10000400)
   dump <from> <to>    Print a hex dump of memory content
   reset               Reset target using DTR line
   terminal            Reset and start a 115200 8N1 terminal w/o flow control,
                       this should be the last action.
                       Currently this terminal is RX only.
Version:
   yalf %(version)s
''' % {"prog" : os.path.basename(sys.argv[0]),
       "version" : version})
    if errmsg:
        stream.write("\nError: %s\n" % (errmsg))
    sys.exit(errmsg and 1 or 0)


###############################################################################
def exec_actions(yalf, actions):
    while len(actions):
        action = actions.popleft()
        if action == 'erase':
            try:
                start = int(actions.popleft(), 0)
                end = int(actions.popleft(), 0)
            except Exception, e:
                sys.stderr.write("\nMissing or wrong arguments after 'erase' (%s)\n" % str(e))
                return 1
            yalf.erase_sectors(start, end)

        elif action == 'eraseall':
            yalf.erase_sectors(0, -1)

        elif action == 'program':
            try:
                filename = actions.popleft()
            except Exception, e:
                sys.stderr.write("\nMissing file name after 'program' (%s)\n" % str(e))
                return 1
            yalf.program_flash(filename)

        elif action == 'ramload':
            try:
                filename = actions.popleft()
            except Exception, e:
                sys.stderr.write("\nMissing file name after 'ramload' (%s)\n" % str(e))
                return 1
            yalf.program_ram(filename)

        elif action == 'go':
            if len(actions) > 0:
                addr = int(actions.popleft(), 0)
            else:
                # read reset vector - doesn't work on LPC17xx
                addr = (yalf.read_mem(4, 7))[0] & ~1
                print "using start address %08x" % addr
            yalf.go(addr)

        elif action == 'dump':
            start = int(actions.popleft(), 0)
            end = int(actions.popleft(), 0)
            mem_content = yalf.read_mem(start, end)
            yalf.print_words(0, mem_content)

        elif action == 'reset':
            yalf.reset()

        elif action == 'terminal':
            yalf.terminal()

        else:
            usage_and_exit("Unknown action '%s'" % action)
    return 0
    
###############################################################################
def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvp:",
                                   ["help", "verbose", "port="])
    except Exception, e:
        sys.stderr.write("\nError: %s\n" % str(e))
        return 1

    # Process options
    verbose = False
    port    = '/dev/ttyS0'
    for opt, value in opts:
        if opt == "--help" or opt == "-h":
            usage_and_exit()
        elif opt == "--verbose" or opt == "-v":
            verbose = True
        elif opt == "--port" or opt == "-p":
            port = value
            print 'port assigned', port
        else:
            usage_and_exit("Unknown option '%s'" % (opt))

    yalf = Yalf(verbose, port, 115200)
    return exec_actions(yalf, deque(args))


if __name__ == '__main__':
    main()
