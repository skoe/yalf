#
# (c) 2011 - 2014 Thomas 'skoe' Giesel
#
# https://bitbucket.org/skoe/yalf
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.

import sys, os, serial
import time
#, binascii, array, getopt
#from collections import deque

import config

class LPCLowLevel:

    def __init__(self, verbose, port, bitrate):
        self._verbose = verbose
        self._port = port
        self._bitrate = bitrate
        self._isp_mode_active = False
        self._ser = None
        self.part_info = None

    ###########################################################################
    def _open_serial(self, xonxoff=True, timeout=0.5):
        try:
            self._ser = serial.Serial(self._port, self._bitrate, 
                                      timeout=timeout, xonxoff=xonxoff)
            # todo: How to find out if the port has been opened?
            self._ser.read(1)
        except Exception, e:
            sys.stderr.write("\nError: %s\n" % str(e))
            exit(1)
        if self._verbose:
            print 'Using port' + str(self._ser.port) + '.'

    ###########################################################################
    def enter_isp_mode(self):
        '''Reset to ISP mode and do the auto-baud stuff.
        
        The reset does only work if DTR and RTS are connected properly.
        Otherwise the user must reset to ISP mode manually.
        Use the auto-baud protocol to synchronize to the device. Try forever
        if no synchronization is possible.
        
        '''
        if self._ser == None:
            self._open_serial()
        print 'Resetting to ISP mode...'
        self._ser.setDTR(True) # reset
        time.sleep(0.01)
        self._ser.setRTS(True) # PIO0_1
        time.sleep(0.01)
        self._ser.setDTR(False)
        time.sleep(0.01)
        self._ser.setRTS(False)
        time.sleep(0.01)
        print 'Synchronizing...'
        self._isp_mode_active = True
        while True:
            self._ser.write('?')
            response = self._ser.readline(20).strip()
            if response == 'Synchronized':
                response = self.send_cmd_recv_response('Synchronized')
                if response == 'OK':
                    break
                else:
                    print 'Synchronization failed. Trying again.'
        self.send_xtal_freq('12000') # ignored on UART anyway
        #self.read_part_id()
        #self.unlock()

    ###########################################################################
    def read_part_id(self):
        '''Read part ID
        
        Read the part ID. If it is known to us, set self.part_info to a
        a chip info dictionary (an entry of _chip_table). Otherwise set it to 
        None.
        
        '''
        if self._verbose:
            print 'Reading part ID...'
        response = self.send_cmd_recv_response('J')
        if self.check_return_code(response):
            id = self.recv_response()
            id = int(id, 0)
            if self._verbose:
                print 'Part ID:', id, '(0x%08x)' % id 
            if id in config.chip_table:
                self.part_info = config.chip_table[id]
                print 'Found', self.part_info['name']
        else:
            print 'Unknown response (' + ret + ').'
            self.part_info = None

    ###########################################################################
    def send_xtal_freq(self, xtal):
        '''Send XTAL frequency
        
        Send the XTAL frequency. Actually this is not used by the UART 
        bootloader but has to be sent nevertheless.
        
        '''
        response = self.send_cmd_recv_response(xtal);
        if response != 'OK':
            print 'Failed to send XTAL frequency.'

    ###########################################################################
    def sector_number_for_offset(self, offset):
        if self.part_info == None:
            self.read_part_id()
        sector_number = 0
        sector_offset = 0
        part_sectors = self.part_info['sectors']
        part_layout = config.flash_layout_table[self.part_info['layout']]
        while sector_number < part_sectors and \
                offset >= sector_offset + part_layout[sector_number]:
            sector_offset += part_layout[sector_number]
            sector_number += 1
        return sector_number

    ###########################################################################
    def recv_response(self):
        '''
        Read a single line response. Skip empty lines until a 
        non-empty line is received.
        '''
        result = ''
        while len(result) == 0:
            result = self._ser.readline(100).strip();
        if self._verbose:
            print 'Received:', result
        return result
    
    
    ###########################################################################
    def send_cmd(self, cmd):
        '''
        Send CMD, check if all characters are echoed correctly.
        Throw an exception if the echo was wrong.
        '''
        if not self._isp_mode_active:
            self.enter_isp_mode()
        if self._verbose:
            print 'Send cmd  :', cmd
        sys.stdout.flush()
        self._ser.write(cmd + '\n')
        echo = self._ser.readline(100)
        if echo != cmd + '\n':
            sys.stderr.write('wrong echo: ' + echo + ' (should be ' + cmd + ')\n')
            exit(1)

    ###########################################################################
    def send_cmd_recv_response(self, cmd):
        '''
        Send CMD, check if all characters are echoed correctly.
        Read the response and return it.
        Throw an exception if the echo was wrong.
        '''
        if not self._isp_mode_active:
            self.enter_isp_mode()
        self.send_cmd(cmd)
        return self.recv_response()
    
    ###########################################################################
    def check_return_code(self, ret):
        i = int(ret)
        if i == 0:
            if self._verbose:
                print 'OK'
            return True
        if i in config.isp_return_codes:
            print config.isp_return_codes[i]
        else:
            print 'Unknown response (' + ret + ').'
        return False

    ###########################################################################
    def get_num_sectors(self):
        if self.part_info == None:
            self.read_part_id()
        return self.part_info['sectors']

    ###########################################################################
    def reset(self):
        if self._ser == None:
            self._open_serial()
        self._ser.setRTS(False) # PIO0_1
        self._ser.setDTR(True) # reset
        time.sleep(0.01)
        self._ser.setDTR(False)

    ###########################################################################
    def terminal(self):
        if self._ser != None:
            self._ser.close()
        self._open_serial(False, None)
        self.reset()
        while True:
            line = self._ser.readline().decode('utf-8', 'ignore')
            print(line.rstrip())
