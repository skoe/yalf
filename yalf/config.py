#
# (c) 2011 - 2014 Thomas 'skoe' Giesel
#
# https://bitbucket.org/skoe/yalf
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.

isp_return_codes = {
    0  : 'CMD_SUCCESS - Command is executed successfully. Sent by ISP '
         'handler only when command given by the host has '
         'been completely and successfully executed.',
    1  : 'INVALID_COMMAND - Invalid command.',
    2  : 'SRC_ADDR_ERROR - Source address is not on word boundary.',
    3  : 'DST_ADDR_ERROR - Destination address is not on a correct ' 
         'boundary.',
    4  : 'SRC_ADDR_NOT_MAPPED - Source address is not mapped in the memory '
         'map. '
         'Count value is taken in to consideration where applicable.',
    5  : 'DST_ADDR_NOT_MAPPED - Destination address is not mapped in the '
         'memory map. Count value is taken in to consideration where '
         'applicable.',
    6  : 'COUNT_ERROR - Byte count is not multiple of 4 or is not a '
         'permitted value.',
    7  : 'INVALID_SECTOR - Sector number is invalid or end sector number '
         'is greater than start sector number.',
    8  : 'SECTOR_NOT_BLANK - Sector is not blank.',
    9  : 'SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION - Command to prepare '
         'sector for write operation was not executed.',
    10 : 'COMPARE_ERROR - Source and destination data not equal.',
    11 : 'BUSY - Flash programming hardware interface is busy.',
    12 : 'PARAM_ERROR - Insufficient number of parameters or invalid '
         'parameter.',
    13 : 'ADDR_ERROR - Address is not on word boundary.',
    14 : 'ADDR_NOT_MAPPED - Address is not mapped in the memory map. '
         'Count value is taken in to consideration where applicable.',
    15 : 'CMD_LOCKED - Command is locked.',
    16 : 'INVALID_CODE - Unlock code is invalid.',
    17 : 'INVALID_BAUD_RATE - Invalid baud rate setting.',
    18 : 'INVALID_STOP_BIT - Invalid stop bit setting.',
    19 : 'CODE_READ_PROTECTION_ENABLED - Code read protection enabled.'
}

# A dictionary from architecture to a list of flash sector sizes
flash_layout_table = {
    '13xx' : (
        4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096
    ),
    '17xx' : (
        4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096,
        4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096,
        32768, 32768, 32768, 32768, 32768,
        32768, 32768, 32768, 32768, 32768,
        32768, 32768, 32768, 32768
    )
}

# A dictionary from part ID to another dictionary which contains 
# information about the part 
chip_table = {
    # LPC 13xx
    0x2C42502B : { 
        'name' : 'LPC1311', 'layout' : '13xx', 'sectors' : 2
    },
    0x2C40102B : { 
        'name' : 'LPC1313', 'layout' : '13xx', 'sectors' : 8
    },
    0x3D01402B : {
        'name' : 'LPC1342', 'layout' : '13xx', 'sectors' : 4
    },
    0x3D00002B : {
        'name' : 'LPC1343', 'layout' : '13xx', 'sectors' : 8
    },

    # LPC 176x
    0x26113f37 : {
        'name' : 'LPC1769', 'layout' : '17xx', 'sectors' : 30
    },
    0x26013f37 : {
        'name' : 'LPC1768', 'layout' : '17xx', 'sectors' : 30
    },
    0x26012837 : {
        'name' : 'LPC1767', 'layout' : '17xx', 'sectors' : 30
    },
    0x26013f33 : {
        'name' : 'LPC1766', 'layout' : '17xx', 'sectors' : 22
    },
    0x26013733 : {
        'name' : 'LPC1765', 'layout' : '17xx', 'sectors' : 22
    },
    0x26011922 : {
        'name' : 'LPC1764', 'layout' : '17xx', 'sectors' : 18
    },

    # LPC 175x
    0x25113737 : {
        'name' : 'LPC1759', 'layout' : '17xx', 'sectors' : 30
    },
    0x25013f37 : {
        'name' : 'LPC1758', 'layout' : '17xx', 'sectors' : 30
    },
    0x25011723 : {
        'name' : 'LPC1756', 'layout' : '17xx', 'sectors' : 22
    },
    0x25011722 : {
        'name' : 'LPC1754', 'layout' : '17xx', 'sectors' : 18
    },
    0x25001121 : {
        'name' : 'LPC1752', 'layout' : '17xx', 'sectors' : 16
    },
    0x25001118 : {
        'name' : 'LPC1751', 'layout' : '17xx', 'sectors' : 8
    },
    0x25001100 : {
        'name' : 'LPC1751', 'layout' : '17xx', 'sectors' : 8
    }

}

